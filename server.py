#!/usr/bin/env python

import socket


TCP_IP = 'localhost'
TCP_PORT = 4600
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(5)

conn, addr = s.accept()
print 'Connection address:', addr
x = 0
while 1:
    x = x +  1
    conn.send("battery level,")  # echo
    conn.send(str(x))
    conn.send("\n")

    data = conn.recv(BUFFER_SIZE)
    if not data: 
    	print "No data received"
    else:
        print "received data: " 
        print data
    
