/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capstone;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import capstone.Conexion;
import java.io.PrintWriter;

/**
 *
 * @author eduards
 */
public class Servidor extends Conexion{
    
    public Servidor() throws IOException{super("servidor");} //Se usa el constructor para servidor de Conexion
    
    public void startServer()//Método para iniciar el servidor
    {
        try
        {
            System.out.println("Esperando..."); //Esperando conexión

            cs = ss.accept(); //Accept comienza el socket y espera una conexión desde un cliente

            System.out.println("Cliente en línea");
            BufferedReader entrada = new BufferedReader(new InputStreamReader(cs.getInputStream()));

            while((mensajeServidor = entrada.readLine()) != null) //Mientras haya mensajes desde el cliente
            {
                PrintWriter outpuStream =  new PrintWriter("out.txt");
                outpuStream.println(mensajeServidor);
                outpuStream.close();
            }

            //System.out.println("Fin de la conexión");

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
}
